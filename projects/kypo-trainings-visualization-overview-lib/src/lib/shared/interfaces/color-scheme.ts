/**
 * Array of hex color strings.
 * deprecated
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ColorScheme extends Array<string> {}
